package be.jschoreels.demo.eventdriven.services.alerter.api.event;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import java.math.BigDecimal;

@JsonDeserialize(builder = AlertCreated.Builder.class)
public class AlertCreated {

    private String id;

    private String userId;

    private String itemId;

    private BigDecimal thresholdPrice;

    private AlertCreated(Builder builder) {
        id = builder.id;
        userId = builder.userId;
        itemId = builder.itemId;
        thresholdPrice = builder.thresholdPrice;
    }

    public String getId() {
        return id;
    }

    public String getUserId() {
        return userId;
    }

    public String getItemId() {
        return itemId;
    }

    public BigDecimal getThresholdPrice() {
        return thresholdPrice;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(AlertCreated copy) {
        Builder builder = new Builder();
        builder.id = copy.getId();
        builder.userId = copy.getUserId();
        builder.itemId = copy.getItemId();
        builder.thresholdPrice = copy.getThresholdPrice();
        return builder;
    }

    @JsonPOJOBuilder
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static final class Builder {
        private String id;
        private String userId;
        private String itemId;
        private BigDecimal thresholdPrice;

        private Builder() {
        }

        public Builder withId(String val) {
            id = val;
            return this;
        }

        public Builder withUserId(String val) {
            userId = val;
            return this;
        }

        public Builder withItemId(String val) {
            itemId = val;
            return this;
        }

        public Builder withThresholdPrice(BigDecimal val) {
            thresholdPrice = val;
            return this;
        }

        public AlertCreated build() {
            return new AlertCreated(this);
        }
    }

    @Override
    public String toString() {
        return "AlertCreated{" +
                "id='" + id + '\'' +
                ", userId='" + userId + '\'' +
                ", itemId='" + itemId + '\'' +
                ", thresholdPrice=" + thresholdPrice +
                '}';
    }
}
