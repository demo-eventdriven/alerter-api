package be.jschoreels.demo.eventdriven.services.alerter.api.event;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.math.BigDecimal;

@JsonDeserialize(builder = AlertDeleted.Builder.class)
public class AlertDeleted {

    private String id;

    private String userId;

    private String itemId;

    private BigDecimal thresholdPrice;

    private AlertDeleted(Builder builder) {
        id = builder.id;
        userId = builder.userId;
        itemId = builder.itemId;
        thresholdPrice = builder.thresholdPrice;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(AlertDeleted copy) {
        Builder builder = new Builder();
        builder.id = copy.getId();
        builder.userId = copy.getUserId();
        builder.itemId = copy.getItemId();
        builder.thresholdPrice = copy.getThresholdPrice();
        return builder;
    }

    public String getId() {
        return id;
    }

    public String getUserId() {
        return userId;
    }

    public String getItemId() {
        return itemId;
    }

    public BigDecimal getThresholdPrice() {
        return thresholdPrice;
    }

    public static final class Builder {
        private String id;
        private String userId;
        private String itemId;
        private BigDecimal thresholdPrice;

        private Builder() {
        }

        public Builder withId(String val) {
            id = val;
            return this;
        }

        public Builder withUserId(String val) {
            userId = val;
            return this;
        }

        public Builder withItemId(String val) {
            itemId = val;
            return this;
        }

        public Builder withThresholdPrice(BigDecimal val) {
            thresholdPrice = val;
            return this;
        }

        public AlertDeleted build() {
            return new AlertDeleted(this);
        }
    }

    @Override
    public String toString() {
        return "AlertDeleted{" +
                "id='" + id + '\'' +
                ", userId='" + userId + '\'' +
                ", itemId='" + itemId + '\'' +
                ", thresholdPrice=" + thresholdPrice +
                '}';
    }
}
