package be.jschoreels.demo.eventdriven.services.alerter.api.event;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import java.time.ZonedDateTime;
import java.util.Optional;

@JsonDeserialize(builder = AlertTriggered.Builder.class)
public class AlertTriggered {

    private String user;

    private ZonedDateTime triggeredTime;

    private Object rootEventPayload;

    private Class rootEventType;

    private AlertTriggered(Builder builder) {
        user = builder.user;
        triggeredTime = builder.triggeredTime;
        rootEventPayload = builder.rootEventPayload;
        rootEventType = Optional.ofNullable(builder.rootEventType).orElse(rootEventPayload.getClass());
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(AlertTriggered copy) {
        Builder builder = new Builder();
        builder.user = copy.getUser();
        builder.triggeredTime = copy.getTriggeredTime();
        builder.rootEventPayload = copy.getRootEventPayload();
        builder.rootEventType = copy.getRootEventType();
        return builder;
    }

    public String getUser() {
        return user;
    }

    public ZonedDateTime getTriggeredTime() {
        return triggeredTime;
    }

    public Object getRootEventPayload() {
        return rootEventPayload;
    }

    public Class getRootEventType() {
        return rootEventType;
    }

    @JsonPOJOBuilder()
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static final class Builder {
        private String user;
        private ZonedDateTime triggeredTime;
        private Object rootEventPayload;
        private Class rootEventType;

        private Builder() {
        }

        public Builder withUser(String val) {
            user = val;
            return this;
        }

        public Builder withTriggeredTime(ZonedDateTime val) {
            triggeredTime = val;
            return this;
        }

        public Builder withRootEventPayload(Object val) {
            rootEventPayload = val;
            return this;
        }

        public Builder withRootEventType(Class val) {
            rootEventType = val;
            return this;
        }

        public AlertTriggered build() {
            return new AlertTriggered(this);
        }
    }


    @Override
    public String toString() {
        return "AlertTriggered{" +
                "user='" + user + '\'' +
                ", triggeredTime=" + triggeredTime +
                ", rootEventPayload=" + rootEventPayload +
                ", rootEventType=" + rootEventType +
                '}';
    }
}